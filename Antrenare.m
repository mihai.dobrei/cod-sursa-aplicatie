trainInputs = xlsread('inputData.xlsx','C2:L236');
trainTargets = xlsread('inputData.xlsx','M2:M236');
testInputs = xlsread('inputData.xlsx','C237:L262');
testTargets = xlsread('inputData.xlsx','M237:M262');
trainIn = transpose(trainInputs);
trainOut = transpose(trainTargets);
testIn = transpose(testInputs);
testOut = transpose(testTargets);
n = length(testOut);
numberOfNets = 1000;
bestAccuracy = 0;

% Choose a Training Function
trainFcn = 'trainlm';  % Levenberg-Marquardt backpropagation.
hiddenLayerSize = 10;

% creates a number of networks and saves the most accurate one
for i=1:numberOfNets
    % Create a Network
    net = fitnet(hiddenLayerSize,trainFcn);
    % Setup Division of Data for Training, Validation, Testing
    net.divideParam.trainRatio = 80/100;
    net.divideParam.valRatio = 20/100;
    net.divideParam.testRatio = 0/100; % the net will be tested with new inputs
    
    % doesn't open the nntraintool window
    net.trainParam.showWindow = false;
    
    % Train the Network
    [net,~] = train(net,trainIn,trainOut);
    
    % Test the network with new data
    newOut = round(net(testIn));
    accuracy = sum(newOut==testOut)/n*100;
    
    % save the net (the best of all created and trained)
    if accuracy>bestAccuracy
        bestNN = net;
        bestAccuracy = accuracy;
        save("bestNN.mat","bestNN","accuracy","hiddenLayerSize")
    end
end
msgbox("The network was trained.");